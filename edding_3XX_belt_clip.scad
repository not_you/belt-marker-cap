//total marker cap height (should be sp1234h+pcs/cts if the other is 0
// otherwise at least sp1234h+pcs+cts+clearance
//plesase enter all values including tollerances

//innitial mating force greatly exceeds that during normal operation
// if this is not desired it is reccomended to  use a water soluble support material and or
//bathe the print in accetone for smoother edges

 cleanup=.0005;

 ch=29.8;
 
sp1h= 7.6;	//part one pen clip height
sp12h=10;	//part 1+2 pen clip height

// the difference between height 3 and 4 is the difference between 
//how far the caliper goes into the cap and how deep it is
sp123h=16;	//part 1+2+3 pen clip height 
sp1234h=25;	//part 1+2+3+4 pen clip height
sd1=13; 	//inner diameter 1 (0)
sd2=12.9; //inner diameter 2 (sp12h-sp1h)

//tolerances set up for nylon 
//(may require some cleanup with a knive or an accetone bath,
// if this is not desired slighly increase the diameters)
sd3=8.1;	//inner diameter 3 (sp123h-sp12h)
sd4=7.9;	//inner diameter 4 (sp1234h-sp123h)

//tolerances set up for nGer(and probably ABS)
//(if print with these or simlar materials the belt clip may not last that long,
//because they're relatively brittle and inflexible.
//though i only tested nGen with a thinner belt attachment)
/*sd3=8.15;	//inner diameter 3 (sp123h-sp12h)
sd4=8.05;	//inner diameter 4 (sp1234h-sp123h)
*/

od=16;//outer diameter

ccdi=10; 	//clip clearance inner
ccdo=13.3; 	//clip clearance outer
ccl=9; //lower edge of clip clearance

bcw=7;//outer belt connector beam width
bcc=4;//inner beld connector beam width

ctbc=3;//clip to belt clearance
baw=16;//belt attachment width
bcsh=2.5;//belt clip strenth horizontal
bcsv=2.5;//belt clip strenth vertical
bt=4;//belt thickness
bw=35;//belt width

pcs=0;//pen cap strenth (must be larger than 0 if slitwidth is smaler than 1 to make sense)
cts=1.5;//cap top strenth
spring_slits=4;//number of spring slits at the top of the cap
slitlenth=1;//in parts of cap width, 1 is r
slitwidth=.5;//gap between sping slits


difference(){
	difference(){
		difference(){
			difference(){
				difference(){
					cylinder(h=ch, d=od);
					translate([0,0,-cleanup]) cylinder(h=sp1h+2*cleanup ,d1=sd1 ,d2=sd2);
				}
				translate([0,0,7.6]) cylinder(h=sp12h-sp1h, d1=sd2,d2=sd3);
			}
			translate([0,0,sp12h-(cleanup/2)])
			cylinder(h=sp1234h-sp12h+cleanup*2, d1=sd3, d2=sd3-(sd3-sd4)/(sp123h-sp12h)*(sp1234h-sp12h));
		}
			union(){
				translate([0,0,ccl+1+cleanup]){
				difference(){
					cylinder(h=ch-sp12h-cts, d=ccdo);
					translate([0,0,0])
					cylinder(h=ch-sp12h-cts, d=ccdi);
				}
				for(i=[1:spring_slits])
				{
					rotate([0,-90,360/spring_slits*i])
					translate([sp1h+pcs/2,(ccdi/2)-(ccdi/2*slitlenth/2),-slitwidth/2])
					linear_extrude(slitwidth)
					square(size=[sp1234h-sp1h+pcs+cleanup,ccdi/2*slitlenth],center=true);
				}
			}
		}
	}
	translate([0,0,sp1234h+pcs+cleanup])
	cylinder(h=ch-sp1234h-pcs-cts,d=ccdo);
}

translate([7,0,ch/2])
rotate([90,90,90]){
	linear_extrude(ctbc)
	difference(){
		square(size=[ch,bcw],center=true);
		square(size=[ch+cleanup,bcc],center=true);
	}
	translate([+((bw+bcsv*2)-ch)/2,0,ctbc]){
		difference(){
			linear_extrude(bcsh*2+bt)
			square(size=[bw+bcsv*2,baw],center=true);
			translate([0,0,bcsh])
			linear_extrude(bt)
			square(size=[bw,baw+cleanup],center=true);
		}
	}
}
	

		
